use crate::errors::advanced_check_error::{AdvancedCheckError, Kind};
use attheme_glossary_parser::glossary::{Contents, Glossary, Paragraph};
use image::ImageError;
use std::path::Path;

type Errors = Vec<AdvancedCheckError>;

pub fn advanced_check(glossary: &Glossary) -> Result<(), Errors> {
    let mut errors = Vec::new();

    glossary.sections.iter().for_each(|section| {
        section.section.contents.iter().for_each(|content| {
            if let Err(err) = check_contents(&section.path, content) {
                errors.extend(err);
            }
        })
    });

    if errors.is_empty() {
        Ok(())
    } else {
        Err(errors)
    }
}

fn check_contents(path: &Path, contents: &Contents) -> Result<(), Errors> {
    let mut errors = Vec::new();

    match contents {
        Contents::Subsection(subsection) => {
            subsection.contents.iter().for_each(|content| {
                if let Err(err) = check_contents(path, content) {
                    errors.extend(err);
                }
            })
        }
        Contents::List { items, .. } => {
            items.iter().flatten().for_each(|item| {
                if let Err(err) = check_contents(path, item) {
                    errors.extend(err);
                }
            });
        }
        Contents::Variable { description, .. } => {
            description.iter().for_each(|contents| {
                if let Err(err) = check_contents(path, contents) {
                    errors.extend(err);
                }
            });
        }
        Contents::Paragraph(paragraph) => {
            return check_paragraph(path, paragraph);
        }
        Contents::Image {
            description,
            url,
            meta,
            ..
        } => {
            if let Err(err) = check_paragraph(path, description) {
                errors.extend(err);
            }

            if let Err(err) = check_image(path, url, meta.line) {
                errors.extend(err);
            }
        }
    }

    if errors.is_empty() {
        Ok(())
    } else {
        Err(errors)
    }
}

fn check_paragraph(_path: &Path, _paragraph: &Paragraph) -> Result<(), Errors> {
    // nothing to check in paragraphs yet
    Ok(())
}

fn check_image(path: &Path, url: &str, line: usize) -> Result<(), Errors> {
    let mut image_path = path.to_owned();
    image_path.pop();
    image_path.push(url);

    match image::open(&image_path) {
        Ok(..) => Ok(()),
        Err(ImageError::IoError(..)) => Err(vec![AdvancedCheckError {
            path: path.to_owned(),
            kind: Kind::AbsentImage { line },
        }]),
        Err(..) => Err(vec![AdvancedCheckError {
            path: image_path,
            kind: Kind::CorruptedImage,
        }]),
    }
}
