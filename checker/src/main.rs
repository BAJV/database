use attheme_glossary_parser::parse;
use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

mod advanced_check;
mod errors;

use advanced_check::advanced_check;

fn main() {
    let database = std::fs::read_dir("../database").unwrap();
    let glossary_dirs = database.filter_map(|entry| {
        let entry = entry.ok()?;
        let file_type = entry.file_type().ok()?;

        if file_type.is_dir() {
            Some(entry)
        } else {
            None
        }
    });

    let mut has_errored = false;

    glossary_dirs.for_each(|glossary_dir| {
        let stderr = &mut StandardStream::stderr(ColorChoice::Always);

        let path = glossary_dir.path();
        write!(stderr, "Checking `{}`... ", path.to_string_lossy()).unwrap();
        stderr.flush().unwrap();

        let mut color = ColorSpec::new();
        color.set_bold(true);

        let mut on_error = || {
            has_errored = true;
            color.set_fg(Some(Color::Red));
            stderr.set_color(&color).unwrap();

            writeln!(stderr, "Failed:").unwrap();
            stderr.set_color(&ColorSpec::new()).unwrap();
        };

        let glossary = match parse(path) {
            Ok(glossary) => glossary,
            Err(errors) => {
                on_error();
                errors.iter().for_each(errors::parse_error::display);
                return;
            }
        };

        if let Err(errors) = advanced_check(&glossary) {
            on_error();
            errors
                .iter()
                .for_each(errors::advanced_check_error::display);
            return;
        }

        color.set_fg(Some(Color::Green));
        stderr.set_color(&color).unwrap();

        writeln!(stderr, "Done").unwrap();
        stderr.set_color(&ColorSpec::new()).unwrap();
    });

    if has_errored {
        std::process::exit(1);
    }
}
